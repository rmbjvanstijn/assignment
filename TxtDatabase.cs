﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using log4net;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace HolidayCottageBooking
{   
    public class TxtDatabase<T>
        where T : class
    {
        private static readonly ILog log
            = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<T> Get()
        {
            var list = this.GetListFromTxtFile(string.Empty);

            return list;
        }

        public bool Add(T obj)
        {
            var list = this.GetListFromTxtFile(string.Empty);
            list.Add(obj);

            var result = WriteListToTextFile(list);
            
            return result;
        }

        public bool Delete(T obj)
        {
            var objectId = obj?.GetType().GetProperty("Id")?.GetValue(obj, null).ToString();
            var list = GetListFromTxtFile(string.Empty);

            foreach (var item in list)
            {
                var itemId = item?.GetType().GetProperty("Id")?.GetValue(item, null).ToString();

                if (objectId == itemId)
                {
                    list.Remove(item);
                    break;
                }
            }

            var result = WriteListToTextFile(list);

            return result;
        }

        public T Update(T obj)
        {
            var objectId = obj?.GetType().GetProperty("Id")?.GetValue(obj, null).ToString();
            var list = GetListFromTxtFile(string.Empty);
            foreach (var item in list)
            {
                var itemId = item?.GetType().GetProperty("Id")?.GetValue(item, null).ToString();

                if (objectId != itemId)
                {
                    continue;
                }
                    
                var index = list.IndexOf(item);
                list[index] = obj;
                break;
            }

            WriteListToTextFile(list);
            return obj;
        }

        private static string GetFilePath()
        {
            var type = typeof(T);
            var dataName = type.Name;
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            var path = $"{basePath}Data\\{dataName}Data.txt";
            return path;
        }

        public List<T> GetListFromTxtFile(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                path = GetFilePath();
            }
            
            // This text is added only once to the file.
            if (!File.Exists(path))
            {
                Console.WriteLine($"{path} doesn't exists, creating new one");
                // Create a file to write to.
                File.WriteAllText(path, "", Encoding.UTF8);
            }

            // Open the file to read from.
            var readText = File.ReadAllText(path);


            if (string.IsNullOrWhiteSpace(readText))
            {
                return new List<T>();
            }
            var errors = new List<string>();
            var jsonSettings = new JsonSerializerSettings
            {
                Error = (sender, args) =>
                {
                    errors.Add(args.ErrorContext.Error.Message);
                    args.ErrorContext.Handled = true;
                },
                Converters = { new IsoDateTimeConverter() }
            };

            var list = JsonConvert.DeserializeObject<List<T>>(readText, jsonSettings);

            if (errors.Any())
            {
                foreach (var error in errors)
                {
                    log.Debug(error); 
                }

                Console.WriteLine($"{path} is not a valid JSON. See logs for more details");
                return new List<T>();
            }

            return list;
        }

        public bool WriteListToTextFile(List<T> list)
        {
            var path = GetFilePath();
            var json = JsonConvert.SerializeObject(list);

            try
            {
                File.WriteAllText(@path, json);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Could not write file {path}. See logs for more details");
                log.Debug(exception);
                return false;
            }
            
            return true;
        }
    }
}
