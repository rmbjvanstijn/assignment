﻿using System;
using System.Collections.Generic;
using System.Linq;

using HolidayCottageBooking.Helpers;
using HolidayCottageBooking.Models;
using HolidayCottageBooking.Store;

namespace HolidayCottageBooking.Manager
{
    public class PriceManager
    {
        private readonly PriceStore priceStore;
        private readonly CottageManager cottageManager;
        private readonly GetDataHelper getDataHelper;
        private readonly UpdateDataHelper updateHelper;

        public PriceManager(
            PriceStore priceStore, 
            GetDataHelper getDataHelper, 
            UpdateDataHelper updateHelper, 
            CottageManager cottageManager)
        {
            this.priceStore = priceStore;
            this.getDataHelper = getDataHelper;
            this.updateHelper = updateHelper;
            this.cottageManager = cottageManager;
        }

        public void GetAllPrices()
        {
            var allPrices = this.priceStore.FindAll();

            if (!this.PricesExists())
            {
                return;
            }

            foreach (var price in allPrices)
            {
                var priceString = price.ToString();
                Console.Write(priceString + Environment.NewLine);
            }
        }

        public void AddPrice()
        {
            var newPrice = this.CreatePriceObject();
            this.priceStore.Add(newPrice);

            Console.WriteLine("Price successfully added" + Environment.NewLine);
        }

        public void UpdatePrice()
        {
            if (!this.PricesExists())
            {
                return;
            }

            var price = this.GetPriceById();

            Console.WriteLine("Change value or press enter");

            var validatedPricePerNight = this.updateHelper.UpdateDecimalValue("Price per night", price.PricePerNight);
            var validatedFromDate = this.updateHelper.UpdateDateTimeValue("From date", price.FromDate);
            var validatedTillDate = this.updateHelper.UpdateDateTimeValue("Till date", price.TillDate);
            var isActive = this.updateHelper.UpdateBoolValue("IsActive", price.IsActive);

            Console.WriteLine($"CottageId: {price.CottageId}");
            var cottageIdValue = Console.ReadLine();
            var cottageId = !string.IsNullOrWhiteSpace(cottageIdValue) ? cottageIdValue : price.CottageId;

            var newPrice = new Price(price.Id, validatedPricePerNight, validatedFromDate, validatedTillDate, isActive, cottageId);

            var result = this.priceStore.Update(newPrice);

            Console.WriteLine("Price successfully updated:");
            var priceString = result.ToString();
            Console.Write(priceString + Environment.NewLine);

        }

        public void RemovePrice()
        {
            if (!this.PricesExists())
            {
                return;
            }

            var price = this.GetPriceById();

            Console.WriteLine(price.ToString());
            Console.WriteLine("Are you sure you want to delete this client (y/n)");
            var anwser = Console.ReadLine();

            if (anwser != "y")
            {
                Console.WriteLine("No price is deleted");
                return;
            }

            this.priceStore.Delete(price);
            Console.WriteLine($"price with Id {price.Id} is succesfully deleted");
        }

        public bool PricesExists()
        {
            var allPrices = this.priceStore.FindAll();

            if (!allPrices.Any())
            {
                Console.WriteLine("There are no prices yet" + Environment.NewLine);
                return false;
            }

            return true;
        }

        public Price GetPriceById()
        {
            while (true)
            {
                Console.WriteLine("Enter price id:" + Environment.NewLine);
                var choosenId = Console.ReadLine();
                var price = this.priceStore.FindById(choosenId);

                if (Equals(price, null))
                {
                    Console.WriteLine($"Cannot find price with id {choosenId}, try another id");
                    continue;
                }

                return price;
            }
        }

        public List<Price> GetPriceByCottageId(string id)
        {
            var price = this.priceStore.FindByCottageId(id);

            return price;
        }

        public Price CreatePriceObject()
        {
            Console.WriteLine("Add values and press enter");

            var pricePerNight = this.getDataHelper.GetDecimal("Price per night (use dot instead of comma):");
            var fromDate = this.getDataHelper.GetDateTime("From date: (use this format: yyyy-mm-dd):");
            var tillDate = this.getDataHelper.GetDateTime("Till date: (use this format: yyyy-mm-dd):");
            var isActive = this.getDataHelper.GetBool("isActive:");
            var cottage = this.cottageManager.GetCottageByName();
            var cottageId = cottage.Id;

            return new Price(null, pricePerNight, fromDate, tillDate, isActive, cottageId);
        }
    }
}
