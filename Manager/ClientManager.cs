﻿using System;
using System.Linq;

using HolidayCottageBooking.Helpers;
using HolidayCottageBooking.Models;
using HolidayCottageBooking.Store;

namespace HolidayCottageBooking.Manager
{
    public class ClientManager
    {
        private readonly ClientStore clientStore;
        private readonly GetDataHelper getDataHelper;
        private readonly UpdateDataHelper updateHelper;

        public ClientManager(ClientStore clientStore, GetDataHelper getDataHelper, UpdateDataHelper updateHelper)
        {
            this.clientStore = clientStore;
            this.getDataHelper = getDataHelper;
            this.updateHelper = updateHelper;
        }

        public void GetAllClients()
        {
            if (!this.CLientsExists())
            {
                return;
            }

            var allClients = this.clientStore.FindAll();

            foreach (var client in allClients)
            {
                var clientString = client.ToString();
                Console.Write(clientString + Environment.NewLine);
            }
        }

        public void AddClient()
        {
            var newClient = this.CreateClientObject();
            this.clientStore.Add(newClient);

            Console.WriteLine("client successfully added");
        }

        public void UpdateClient()
        {
            if (!this.CLientsExists())
            {
                return;
            }

            var client = this.GetClientByEmail();

            Console.WriteLine("Change value or press enter");
            var firstName = this.updateHelper.UpdateStringValue("Firstname", client.FirstName);
            var lastName = this.updateHelper.UpdateStringValue("Lastname", client.LastName);
            var email = this.updateHelper.UpdateStringValue("Email", client.Email);
            var dateOfBirth = this.updateHelper.UpdateDateTimeValue("Date of birth", client.DateOfBirth);
            var newBookingsList = this.updateHelper.UpdateListString("booking", client.Bookings);

            var newClient = new Client(client.Id, firstName, lastName, email, dateOfBirth, newBookingsList);

            var result = this.clientStore.Update(newClient);

            Console.WriteLine("Client successfully updated:");
            var clientString = result.ToString();
            Console.Write(clientString + Environment.NewLine);

        }

        public void RemoveClient()
        {
            if (!this.CLientsExists())
            {
                return;
            }
       
            var client = this.GetClientByEmail();

            Console.WriteLine(client.ToString());
            Console.WriteLine("Are you sure you want to delete this client (y/n)");
            var anwser = Console.ReadLine();

            if (anwser != "y")
            {
                Console.WriteLine("No client is deleted");
                return;
            }

            this.clientStore.Delete(client);
            Console.WriteLine($"client with name {client.FirstName} {client.LastName} is succesfully deleted");
        }

        public void ImportClientsFromFile()
        {
            var path = $"{AppDomain.CurrentDomain.BaseDirectory}input.txt";
            Console.Write($"adding new clients from {path}" + Environment.NewLine);

            var importedClientList = this.clientStore.GetListFromPath(path);

            if (!importedClientList.Any())
            {
                Console.WriteLine("File is empty");
                return;
            }
            foreach (var item in importedClientList)
            {
                this.clientStore.Add(item);
            }
        }

        public bool CLientsExists()
        {
            var allPrices = this.clientStore.FindAll();

            if (!allPrices.Any())
            {
                Console.WriteLine("There are no clients yet" + Environment.NewLine);
                return false;
            }

            return true;
        }

        public Client GetClientByEmail()
        {
            while (true)
            {
                Console.WriteLine("Enter client email:");
                var choosenEmail = Console.ReadLine();
                var client = this.clientStore.FindByEmail(choosenEmail);

                if (Equals(client, null))
                {
                    Console.WriteLine($"Cannot find client with email {choosenEmail}, try another email");
                    continue;
                }

                return client;
            }
        }

        public Client CreateClientObject()
        {
            Console.WriteLine("Add values and press enter");
            var firstName = this.getDataHelper.GetString("FirstName");
            var lastName = this.getDataHelper.GetString("LastName");
            var email = this.getDataHelper.GetString("Email");
            var dateOfBirth = this.getDataHelper.GetDateTime("Date of birth");
            return new Client(null, firstName, lastName, email, dateOfBirth, new NewList<string>());
        }
    }
}
