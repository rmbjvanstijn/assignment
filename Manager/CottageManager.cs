﻿using System;
using System.Linq;

using HolidayCottageBooking.Helpers;
using HolidayCottageBooking.Models;
using HolidayCottageBooking.Store;

namespace HolidayCottageBooking.Manager
{
    public class CottageManager
    {
        private readonly CottageStore cottageStore;
        private readonly GetDataHelper getDataHelper;
        private readonly UpdateDataHelper updateHelper;

        public CottageManager(CottageStore cottageStore, GetDataHelper getDataHelper, UpdateDataHelper updateHelper)
        {
            this.cottageStore = cottageStore;
            this.getDataHelper = getDataHelper;
            this.updateHelper = updateHelper;
        }

        public void GetAllCottages()
        {
            if (!this.CottagesExists())
            {
                return;
            }

            var allCottages = this.cottageStore.FindAll();

            foreach (var cottage in allCottages)
            {
                var cottageString = cottage.ToString();
                Console.Write(cottageString + Environment.NewLine);
            }
        }

        public void AddCottage()
        {
            var cottage = this.CreateCottageObject();

            this.cottageStore.Add(cottage);

            Console.WriteLine("Cottage successfully added");
        }

        public void UpdateCottage()
        {
            if (!this.CottagesExists())
            {
                return;
            }

            var cottage = this.GetCottageByName();

            Console.WriteLine("Change value or press enter");

            var name = this.updateHelper.UpdateStringValue("Name", cottage.Name);
            var description = this.updateHelper.UpdateStringValue("Description", cottage.Description);
            var numberOfBeds = this.updateHelper.UpdateIntValue("Number of beds", cottage.NumberOfBeds);
            var isActive = this.updateHelper.UpdateBoolValue("IsActive", cottage.IsActive);
            var rating = this.updateHelper.UpdateIntValue("Rating", cottage.Rating);
            var newFacilitiesList = this.updateHelper.UpdateListString("facility", cottage.Facilities);

            var updatedCottage = new Cottage(cottage.Id, name, description, numberOfBeds, rating, newFacilitiesList, isActive);

            var result = this.cottageStore.Update(updatedCottage);

            Console.WriteLine("Cottage successfully updated:");
            var cottageString = result.ToString();
            Console.Write(cottageString + Environment.NewLine);

        }

        public void RemoveCottage()
        {
            if (!this.CottagesExists())
            {
                return;
            }

            var cottage = this.GetCottageByName();

            Console.WriteLine(cottage.ToString());
            Console.WriteLine("Are you sure you want to delete this cottage (y/n)");
            var anwser = Console.ReadLine();

            if (anwser != "y")
            {
                Console.WriteLine("No cottage is deleted");
                return;
            }

            this.cottageStore.Delete(cottage);
            Console.WriteLine($"cottage with name {cottage.Name} is succesfully deleted");
        }
        public bool CottagesExists()
        {
            var allCottages = this.cottageStore.FindAll();

            if (!allCottages.Any())
            {
                Console.WriteLine("There are no cottages yet" + Environment.NewLine);
                return false;
            }

            return true;
        }

        public Cottage GetCottageByName()
        {
            while (true)
            {
                Console.WriteLine("Enter cottage name:");
                var choosenName = Console.ReadLine();
                var cottage = this.cottageStore.FindByName(choosenName);

                if (Equals(cottage, null))
                {
                    Console.WriteLine($"Cannot find cottage with name {choosenName}, try another name");
                    continue;
                }

                return cottage;
            }
        }

        public Cottage CreateCottageObject()
        {
            Console.WriteLine("Add values and press enter");
            var name = this.getDataHelper.GetString("Name:");
            var description = this.getDataHelper.GetString("Description:");
            var numberOfBeds = this.getDataHelper.GetInt("Number of beds");
            var isActive = this.getDataHelper.GetBool("isActive:");

            var facilities = new NewList<string>();

            Console.WriteLine($"Facilities. Add value, then hit enter and add new value. When you are done type 'done'");
            while (true)
            {
                var value = Console.ReadLine();

                if (value == "done")
                {
                    break;
                }

                facilities.Add(value);
            }

            return new Cottage(null, name, description, numberOfBeds, 0, facilities, isActive);
        }
    }
}
