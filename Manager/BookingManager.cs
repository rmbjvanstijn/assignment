﻿using System;
using System.Linq;

using HolidayCottageBooking.Helpers;
using HolidayCottageBooking.Models;
using HolidayCottageBooking.PriceCalculator;
using HolidayCottageBooking.Store;

namespace HolidayCottageBooking.Manager
{
    public class BookingManager
    {
        private readonly CottageManager cottageManager;
        private readonly ClientManager clientManager;
        private readonly BookingStore bookingStore;
        private readonly GetDataHelper getDataHelper;
        private readonly UpdateDataHelper updateHelper;
        private readonly CottagePriceCalculator<Cottage> cottagePriceCalculator;

        public BookingManager(
            BookingStore bookingStore, 
            GetDataHelper getDataHelper, 
            UpdateDataHelper updateHelper, 
            CottageManager cottageManager, 
            ClientManager clientManager,
            CottagePriceCalculator<Cottage> cottagePriceCalculator)
        {
            this.bookingStore = bookingStore;
            this.getDataHelper = getDataHelper;
            this.updateHelper = updateHelper;
            this.cottageManager = cottageManager;
            this.clientManager = clientManager;
            this.cottagePriceCalculator = cottagePriceCalculator;
        }

        public void GetAllBookings()
        {
            if (!this.BookingsExists())
            {
                return;
            }

            var allBookings = this.bookingStore.FindAll();

            foreach (var booking in allBookings)
            {
                var bookingString = booking.ToString();
                Console.Write(bookingString + Environment.NewLine);
            }
        }

        public void AddBooking()
        {
            var booking = this.CreateBookingObject();

            this.bookingStore.Add(booking);

            Console.WriteLine("Booking successfully added");
        }

        public void UpdateBooking()
        {
            if (!this.BookingsExists())
            {
                return;
            }

            var booking = this.GetBookingById();

            Console.WriteLine("Change value or press enter");

            var note = this.updateHelper.UpdateStringValue("Note", booking.Note);
            var fromDate = this.updateHelper.UpdateDateTimeValue("From date", booking.FromDate);
            var tillDate = this.updateHelper.UpdateDateTimeValue("Till date", booking.TillDate);
            var rate = this.updateHelper.UpdateIntValue("Rate", booking.Rate);
            var isActive = this.updateHelper.UpdateBoolValue("IsActive", booking.IsActive);
            var isPending = this.updateHelper.UpdateBoolValue("IsPending", booking.IsPending);
            var isPayed = this.updateHelper.UpdateBoolValue("IsPayed", booking.IsPayed);
            var cottage = this.cottageManager.GetCottageByName();
            var client = this.clientManager.GetClientByEmail();
            var price = this.cottagePriceCalculator.GetTotalBookingsPrice(fromDate, tillDate, cottage);

            var updatedBooking = new Booking(booking.Id, note, fromDate, tillDate, rate, isActive, isPending, isPayed, cottage, client, price);

            var result = this.bookingStore.Update(updatedBooking);

            Console.WriteLine("Booking successfully updated:");
            var bookingString = result.ToString();
            Console.Write(bookingString + Environment.NewLine);
        }

        public void RemoveBooking()
        {
            if (!this.BookingsExists())
            {
                return;
            }

            var booking = this.GetBookingById();

            Console.WriteLine(booking.ToString());
            Console.WriteLine("Are you sure you want to delete this booking (y/n)");
            var anwser = Console.ReadLine();

            if (anwser != "y")
            {
                Console.WriteLine("No booking is deleted");
                return;
            }

            this.bookingStore.Delete(booking);
            Console.WriteLine($"booking with id {booking.Id} is succesfully deleted");
        }

        public bool BookingsExists()
        {
            var allPrices = this.bookingStore.FindAll();

            if (!allPrices.Any())
            {
                Console.WriteLine("There are no bookings yet" + Environment.NewLine);
                return false;
            }

            return true;
        }

        public Booking GetBookingById()
        {
            while (true)
            {
                Console.WriteLine("Enter Booking id:");
                var choosenId = Console.ReadLine();
                var booking = this.bookingStore.FindByid(choosenId);

                if (Equals(booking, null))
                {
                    Console.WriteLine($"Cannot find booking with id {choosenId}, try another name");
                    continue;
                }

                return booking;
            }
        }

        public Booking CreateBookingObject()
        {
            Console.WriteLine("Add values and press enter");
            var note = this.getDataHelper.GetString("Note");
            var fromDate = this.getDataHelper.GetDateTime("From date");
            var tillDate = this.getDataHelper.GetDateTime("Till date");
            var rate = this.getDataHelper.GetInt("Rate");
            var isActive = this.getDataHelper.GetBool("IsActive");
            var isPending = this.getDataHelper.GetBool("IsPending");
            var isPayed = this.getDataHelper.GetBool("IsPayed");
            var cottage = this.cottageManager.GetCottageByName();
            var client = this.clientManager.GetClientByEmail();
            var price = this.cottagePriceCalculator.GetTotalBookingsPrice(fromDate, tillDate, cottage);

            return new Booking(null, note, fromDate, tillDate, rate, isActive, isPending, isPayed, cottage, client, price);
        }
    }
}
