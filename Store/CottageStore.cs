﻿using System;
using System.Collections.Generic;
using System.Linq;

using HolidayCottageBooking.Models;

namespace HolidayCottageBooking.Store
{
    public class CottageStore
    {
        private readonly TxtDatabase<Cottage> txtDatabase;

        public CottageStore(TxtDatabase<Cottage> txtDatabase)
        {
            this.txtDatabase = txtDatabase;
        }

        public List<Cottage> FindAll()
        {
            var findAllCottages = this.txtDatabase.Get();
            return findAllCottages;
        }

        public Cottage FindByName(string name)
        {
            var findAllCottages = this.txtDatabase.Get();

            var cottage = findAllCottages.FirstOrDefault(x => x.Name == name);
            return cottage;
        }

        public Cottage Update(Cottage client)
        {
            var findAllCottages = this.txtDatabase.Get();
            var cottageToUpdate = findAllCottages.FirstOrDefault(x => x.Id == client.Id);
            if (cottageToUpdate == null)
            {
                Console.WriteLine($"Could not find cottage with id {client.Id}");
            }

            var result = this.txtDatabase.Update(client);

            return result;
        }

        public bool Delete(Cottage cottage)
        {
            var result = this.txtDatabase.Delete(cottage);
            return result;
        }

        public bool Add(Cottage cottage)
        {
            var findAllCottages = this.txtDatabase.Get();

            var existingCottages = findAllCottages.Where(x => x == cottage);

            if (existingCottages.Any())
            {
                return true;
            }

            var result = this.txtDatabase.Add(cottage);
            return result;
        }
    }
}
