﻿using System;
using System.Collections.Generic;
using System.Linq;

using HolidayCottageBooking.Models;

namespace HolidayCottageBooking.Store
{
    public class ClientStore
    {
        private readonly TxtDatabase<Client> txtDatabase;

        public ClientStore(TxtDatabase<Client> txtDatabase)
        {
            this.txtDatabase = txtDatabase;
        }

        public List<Client> FindAll()
        {
            var findAllClients = this.txtDatabase.Get();

            return findAllClients;
        }

        public Client FindByEmail(string email)
        {
            var findAllClients = this.txtDatabase.Get();

            var client = findAllClients.FirstOrDefault(x => x.Email == email);
            return client;
        }

        public Client Update(Client client)
        {
            var findAllClients = this.txtDatabase.Get();
            var clientToUpdate = findAllClients.FirstOrDefault(x => x.Id == client.Id);
            if (clientToUpdate == null)
            {
                Console.WriteLine($"Could not find client with id {client.Id}");
            }

            var result = this.txtDatabase.Update(client);

            return result;
        }

        public bool Delete(Client client)
        {
            var result = this.txtDatabase.Delete(client);
            return result;
        }

        public bool Add(Client client)
        {
            var findAllClients = this.txtDatabase.Get();

            var existingClients = findAllClients.Where(x => x == client);

            if (existingClients.Any())
            {
                Console.WriteLine($"{client.Email} already exists");
                return true;
            }

            var result = this.txtDatabase.Add(client);

            Console.WriteLine("Clients succefully added");
            return result;
        }

        public List<Client> GetListFromPath(string path)
        {
            var list = this.txtDatabase.GetListFromTxtFile(path);
            return list;

        }

    }
}
