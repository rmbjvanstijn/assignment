﻿using System;
using System.Collections.Generic;
using System.Linq;

using HolidayCottageBooking.Models;

namespace HolidayCottageBooking.Store
{
    public class PriceStore
    {
        private readonly TxtDatabase<Price> txtDatabase;

        public PriceStore(TxtDatabase<Price> txtDatabase)
        {
            this.txtDatabase = txtDatabase;
        }

        public List<Price> FindAll()
        {
            var findAllPrices = this.txtDatabase.Get();
            return findAllPrices;
        }

        public Price FindById(string id)
        {
            var findAllPrices = this.txtDatabase.Get();

            var price = findAllPrices.FirstOrDefault(x => x.Id == id);
            return price;
        }

        public List<Price> FindByCottageId(string id)
        {
            var findAllPrices = this.txtDatabase.Get();
            var prices = findAllPrices.Where(x => x.CottageId == id).ToList();
            return prices;
        }

        public Price Update(Price client)
        {
            var findAllPrices = this.txtDatabase.Get();
            var priceToUpdate = findAllPrices.FirstOrDefault(x => x.Id == client.Id);
            if (priceToUpdate == null)
            {
                Console.WriteLine($"Could not find price with id {client.Id}");
            }

            var result = this.txtDatabase.Update(client);

            return result;
        }

        public bool Delete(Price price)
        {
            var result = this.txtDatabase.Delete(price);
            return result;
        }

        public bool Add(Price price)
        {
            var findAllPrices = this.txtDatabase.Get();

            var existingPrices = findAllPrices.Where(x => x == price);

            if (existingPrices.Any())
            {
                return true;
            }

            var result = this.txtDatabase.Add(price);
            return result;
        }
    }
}
