﻿using System;
using System.Collections.Generic;
using System.Linq;

using HolidayCottageBooking.Models;

namespace HolidayCottageBooking.Store
{
    public class BookingStore
    {
        private readonly TxtDatabase<Booking> txtDatabase;

        public BookingStore(TxtDatabase<Booking> txtDatabase)
        {
            this.txtDatabase = txtDatabase;
        }

        public List<Booking> FindAll()
        {
            var findAllBookings = this.txtDatabase.Get();
            return findAllBookings;
        }

        public Booking FindByid(string id)
        {
            var findAllBookings = this.txtDatabase.Get();

            var booking = findAllBookings.FirstOrDefault(x => x.Id == id);
            return booking;
        }

        public Booking Update(Booking client)
        {
            var findAllBookings = this.txtDatabase.Get();
            var bookingToUpdate = findAllBookings.FirstOrDefault(x => x.Id == client.Id);
            if (bookingToUpdate == null)
            {
                Console.WriteLine($"Could not find booking with id {client.Id}");
            }

            var result = this.txtDatabase.Update(client);

            return result;
        }

        public bool Delete(Booking booking)
        {
            var result = this.txtDatabase.Delete(booking);
            return result;
        }

        public bool Add(Booking booking)
        {
            var findAllBookings = this.txtDatabase.Get();

            var existingBookings = findAllBookings.Where(x => x == booking);

            if (existingBookings.Any())
            {
                return true;
            }

            var result = this.txtDatabase.Add(booking);
            return result;
        }
    }
}
