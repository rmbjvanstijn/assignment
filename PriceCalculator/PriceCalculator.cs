﻿using System;

namespace HolidayCottageBooking.PriceCalculator
{
    public abstract class PriceCalculator<TStay>
    {
        public abstract decimal GetTotalBookingsPrice(DateTime fromDate, DateTime tillDate, TStay stay);
    }
}
