﻿using System;
using System.Linq;

using HolidayCottageBooking.Manager;
using HolidayCottageBooking.Models;

namespace HolidayCottageBooking.PriceCalculator
{
    public class CottagePriceCalculator<TStay> : PriceCalculator<TStay>
        where TStay : Cottage
    {
        private readonly PriceManager priceManager;

        public CottagePriceCalculator(PriceManager priceManager)
        {
            this.priceManager = priceManager;
        }

        //This calculation assumes that all price date ranges are in order
        //so when on the date of a tillDate a priceobject with datefrom on the same date is available
        public override decimal GetTotalBookingsPrice(DateTime fromDate, DateTime tillDate, TStay stay)
        {
            var cottagePrices = this.priceManager.GetPriceByCottageId(stay.Id);

            decimal price = 0;
            var daysBetweenFromAndTill = (tillDate - fromDate).Days;
            var daysLeft = daysBetweenFromAndTill;

            var cottagePrice = cottagePrices.OrderByDescending(x => x.FromDate).FirstOrDefault(x => x.FromDate <= fromDate && x.TillDate > fromDate);

            if (cottagePrice == null)
            {
                Console.WriteLine($"Cottage {stay.Name} misses price ranges between {fromDate} an {tillDate}");
                decimal result = 0;
                return result;
            }

            //Date range is in one priceObject
            if (cottagePrice.TillDate > tillDate)
            {
                price = (decimal)daysBetweenFromAndTill * cottagePrice.PricePerNight;
                return price;
            }

            //
            if (cottagePrice.TillDate < tillDate)
            {
                var calculatedDays = (cottagePrice.TillDate - fromDate).Days;
                var calculatePrice = calculatedDays * cottagePrice.PricePerNight;
                price += calculatePrice;
                daysLeft -= calculatedDays;
            }

            //Gets all prices orderedBy based on first priceobject after de first used priceobject fromdate
            var orderedCottagePrices = cottagePrices.OrderBy(x => x.FromDate).Where(x => x.FromDate > cottagePrice.FromDate).ToList();
            var i = 0;

            while (true)
            {
                var orderedCottagePrice = orderedCottagePrices[i];
                var dayDifference = (orderedCottagePrice.TillDate - orderedCottagePrice.FromDate).Days;

                if (dayDifference > daysLeft)
                {
                    price += daysLeft * orderedCottagePrice.PricePerNight;
                    break;
                }

                daysLeft -= dayDifference;
                price += dayDifference * orderedCottagePrice.PricePerNight;

                i++;
            }

            return price;
        }
    }
}
