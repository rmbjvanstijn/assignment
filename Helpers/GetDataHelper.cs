﻿using System;

namespace HolidayCottageBooking.Helpers
{
    public class GetDataHelper
    {
        private readonly Validators validators;

        public GetDataHelper(Validators validators)
        {
            this.validators = validators;
        }

        public string GetString(string propertyToUpdate)
        {
            Console.WriteLine(propertyToUpdate);
            var stringValue = Console.ReadLine();
            return stringValue;
        }

        public int GetInt(string propertyToUpdate)
        {
            Console.WriteLine(propertyToUpdate);
            var intValue = Console.ReadLine();
            return this.validators.StringToIntValidator(intValue);
        }

        public decimal GetDecimal(string propertyToUpdate)
        {
            Console.WriteLine(propertyToUpdate);
            var decimalValue = Console.ReadLine();
            return this.validators.StringToDecimalValidator(decimalValue);
        }

        public DateTime GetDateTime(string propertyToUpdate)
        {
            Console.WriteLine($"{propertyToUpdate}: (use this format: yyyy-mm-dd)");
            var dateValue = Console.ReadLine();
            return this.validators.StringToDateTimeValidator(dateValue);
        }

        public bool GetBool(string propertyToUpdate)
        {
            Console.WriteLine($"{propertyToUpdate}: type y/n");
            var propertyIsActive = Console.ReadLine();
            return propertyIsActive == "y";
        }
    }
}
