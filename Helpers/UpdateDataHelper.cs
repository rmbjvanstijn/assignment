﻿using System;

using HolidayCottageBooking.Models;

namespace HolidayCottageBooking.Helpers
{
    public class UpdateDataHelper
    {
        private readonly Validators validators;

        public UpdateDataHelper(Validators validators)
        {
            this.validators = validators;
        }

        public decimal UpdateDecimalValue(string propertyToUpdate, decimal value)
        {
            Console.WriteLine($"{propertyToUpdate}: {value}");
            var newDecimal = Console.ReadLine();
            var newDecimalValue = !string.IsNullOrWhiteSpace(newDecimal) ? newDecimal : value.ToString();
            return this.validators.StringToDecimalValidator(newDecimalValue);
        }

        public string UpdateStringValue(string propertyToUpdate, string stringValue)
        {
            Console.WriteLine($"{propertyToUpdate}: {stringValue}");
            var newString = Console.ReadLine();
            return !string.IsNullOrWhiteSpace(newString) ? newString : stringValue;
        }

        public int UpdateIntValue(string propertyToUpdate, int intValue)
        {
            Console.WriteLine($"{propertyToUpdate}: {intValue}");
            var newInt = Console.ReadLine();
            var newIntValue = !string.IsNullOrWhiteSpace(newInt) ? newInt : intValue.ToString("yyyy-MM-dd");
            return this.validators.StringToIntValidator(newIntValue);
        }

        public DateTime UpdateDateTimeValue(string propertyToUpdate, DateTime dateValue)
        {
            Console.WriteLine($"{propertyToUpdate}: {dateValue}");
            var newDate = Console.ReadLine();
            var newDateValue = !string.IsNullOrWhiteSpace(newDate) ? newDate : dateValue.ToString("yyyy-MM-dd");
            return this.validators.StringToDateTimeValidator(newDateValue);
        }

        public bool UpdateBoolValue(string propertyToUpdate, bool boolValue)
        {
            Console.WriteLine($"{propertyToUpdate}: {boolValue}. type y/n");
            var propertyIsTrue = Console.ReadLine();
            return propertyIsTrue == "y";
        }

        public NewList<string> UpdateListString(string propertyToUpdate, NewList<string> listValue)
        {
            var list = new NewList<string>();
            foreach (var item in list)
            {
                Console.WriteLine($"Do you want to delete this {propertyToUpdate}: {item}, Type y/n");
                var anwser = Console.ReadLine();

                if (anwser == "n")
                {
                    list.Add(item);
                }
            }

            Console.WriteLine($"Add new {propertyToUpdate} value, then hit enter and add new value. When you are done type 'done'");
            while (true)
            {
                var value = Console.ReadLine();

                if (value == "done")
                {
                    break;
                }

                list.Add(value);
            }

            return list;}
    }
}
