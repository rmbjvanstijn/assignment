﻿using System;

using HolidayCottageBooking.Helpers;
using HolidayCottageBooking.Manager;
using HolidayCottageBooking.Models;
using HolidayCottageBooking.PriceCalculator;
using HolidayCottageBooking.Store;

namespace HolidayCottageBooking
{
    class Program
    {
        private static readonly Validators validators = new Validators();
        private static readonly GetDataHelper getDataHelper = new GetDataHelper(validators);
        private static readonly UpdateDataHelper updateDataHelper = new UpdateDataHelper(validators);
        private static readonly CottageManager cottageManager = new CottageManager(new CottageStore(new TxtDatabase<Cottage>()), getDataHelper, updateDataHelper);
        private static readonly PriceManager priceManager = new PriceManager(new PriceStore(new TxtDatabase<Price>()), getDataHelper, updateDataHelper, cottageManager);
        private static readonly CottagePriceCalculator<Cottage> cottagePriceCalculator = new CottagePriceCalculator<Cottage>(priceManager);
        private static readonly ClientManager clientManager = new ClientManager(new ClientStore(new TxtDatabase<Client>()), getDataHelper, updateDataHelper);
        private static readonly BookingManager bookingManager = new BookingManager(new BookingStore(new TxtDatabase<Booking>()), getDataHelper, updateDataHelper, cottageManager, clientManager, cottagePriceCalculator);

        static void Main(string[] args)
        {
            var showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }

        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1) Clientmenu");
            Console.WriteLine("2) Pricemenu");
            Console.WriteLine("3) Cottagemenu");
            Console.WriteLine("4) Bookingmenu");
            Console.WriteLine("5) Close application");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    CLientMenu();
                    return true;
                case "2":
                    PriceMenu();
                    return true;
                case "3":
                    CottageMenu();
                    return true;
                case "4":
                    BookingMenu();
                    return true;
                case "5":
                    return false;
                default:
                    return true;
            }
        }

        private static void CLientMenu()
        {
            var menuIsActive = true;
            while (menuIsActive)
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1) Show all clients");
                Console.WriteLine("2) Edit a client");
                Console.WriteLine("3) Remove a client");
                Console.WriteLine("4) Add a client");
                Console.WriteLine("5) import clients from file");
                Console.WriteLine("6) Go back to main menu");
                Console.Write("\r\nSelect an option: ");

                switch (Console.ReadLine())
                {
                    case "1":
                        clientManager.GetAllClients();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "2":
                        clientManager.UpdateClient();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "3":
                        clientManager.RemoveClient();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "4":
                        clientManager.AddClient();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "5":
                        clientManager.ImportClientsFromFile();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "6":
                        menuIsActive = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private static void PriceMenu()
        {
            var menuIsActive = true;
            while (menuIsActive)
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1) Show all prices");
                Console.WriteLine("2) Edit a prices");
                Console.WriteLine("3) Remove a prices");
                Console.WriteLine("4) Add a prices");
                Console.WriteLine("5) Go back to main menu");
                Console.Write("\r\nSelect an option: ");

                switch (Console.ReadLine())
                {
                    case "1":
                        priceManager.GetAllPrices();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "2":
                        priceManager.UpdatePrice();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "3":
                        priceManager.RemovePrice();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "4":
                        priceManager.AddPrice();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "5":
                        menuIsActive = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private static void CottageMenu()
        {
            var menuIsActive = true;
            while (menuIsActive)
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1) Show all cottages");
                Console.WriteLine("2) Edit a cottages");
                Console.WriteLine("3) Remove a cottages");
                Console.WriteLine("4) Add a cottages");
                Console.WriteLine("5) Go back to main menu");
                Console.Write("\r\nSelect an option: ");

                switch (Console.ReadLine())
                {
                    case "1":
                        cottageManager.GetAllCottages();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "2":
                        cottageManager.UpdateCottage();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "3":
                        cottageManager.RemoveCottage();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "4":
                        cottageManager.AddCottage();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "5":
                        menuIsActive = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private static void BookingMenu()
        {
            var menuIsActive = true;
            while (menuIsActive)
            {
                Console.Clear();
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1) Show all bookings");
                Console.WriteLine("2) Edit a bookings");
                Console.WriteLine("3) Remove a bookings");
                Console.WriteLine("4) Add a bookings");
                Console.WriteLine("5) Go back to main menu");
                Console.Write("\r\nSelect an option: ");

                switch (Console.ReadLine())
                {
                    case "1":
                        bookingManager.GetAllBookings();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "2":
                        bookingManager.UpdateBooking();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "3":
                        bookingManager.RemoveBooking();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "4":
                        bookingManager.AddBooking();
                        Console.WriteLine("Hit enter to go back to menu");
                        Console.ReadLine();
                        break;
                    case "5":
                        menuIsActive = false;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
