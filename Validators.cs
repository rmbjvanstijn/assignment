﻿using System;

namespace HolidayCottageBooking
{
    public class Validators
    {
        public int StringToIntValidator(string value)
        {
            try
            {
                var result = int.Parse(value);
                return result;

            }
            catch
            {
                return 0;
            }
        }

        public decimal StringToDecimalValidator(string value)
        {
            decimal result;
            try
            {
                result = decimal.Parse(value);
            }
            catch
            {
                result = (decimal)0.00;
            }

            return result;
        }

        public DateTime StringToDateTimeValidator(string value)
        {
            DateTime result;

            try
            {
                result = DateTime.ParseExact(value, "yyyy-MM-dd", null);
            }
            catch
            {
                result = DateTime.Now;
            }

            return result;
        }
    }
}
