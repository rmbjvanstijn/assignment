﻿using System;
using System.Text;

namespace HolidayCottageBooking.Models
{
    public class Client
    {
        private readonly string id;
        private readonly string firstName;
        private readonly string lastName;
        private readonly string email;
        private readonly DateTime dateOfBirth;
        private readonly NewList<string> bookings;

        public Client(
            string id,
            string firstName,
            string lastName,
            string email,
            DateTime dateOfBirth,
            NewList<string> bookings)
        {
            this.id = !string.IsNullOrWhiteSpace(id) ? id : Guid.NewGuid().ToString("N");
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.dateOfBirth = dateOfBirth;
            this.bookings = bookings;
        }

        public string Id => id;
        public string FirstName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(firstName))
                {
                    throw new ArgumentException($"Firstname cannot be empty");
                }
                return firstName;
            }
        }
        public string LastName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(lastName))
                {
                    throw new ArgumentException($"Lastname cannot be empty");
                }
                return lastName;
            }
        }
        public string Email
        {
            get
            {
                if (string.IsNullOrWhiteSpace(email))
                {
                    throw new ArgumentException($"Email cannot be empty");
                }
                return email;
            }
        }
        public DateTime DateOfBirth => dateOfBirth;
        public NewList<string> Bookings => bookings;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Id: {this.Id},");
            sb.AppendLine($"Firstname: {this.FirstName},");
            sb.AppendLine($"Lastname: {this.LastName},");
            sb.AppendLine($"Email: {this.Email},");
            sb.AppendLine($"Date of birth: {this.DateOfBirth},");
            sb.AppendLine($"Bookings: [{this.Bookings}]");
            return sb.ToString();
        }

        public static bool operator ==(Client x, Client y)
        {
            var objectIsNull = Equals(x, null) || Equals(y, null);

            if (objectIsNull)
            {
                return false;
            }

            return string.Equals(x.FirstName, y.FirstName) && string.Equals(x.LastName, y.LastName) && string.Equals(x.Email, y.Email) && x.DateOfBirth.Equals(y.DateOfBirth);
        }

        public static bool operator !=(Client left, Client right)
        {
            return !(left == right);
        }

        protected bool Equals(Client other)
        {
            return other == this;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != this.GetType())
                return false;
            return Equals((Client)obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Email != null ? Email.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ DateOfBirth.GetHashCode();
                return hashCode;
            }
        }
    }
}
