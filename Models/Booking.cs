﻿using System;
using System.Text;

namespace HolidayCottageBooking.Models
{
    public class Booking
    {
        private readonly string id;
        private readonly string note;
        private readonly DateTime fromDate;
        private readonly DateTime tillDate;
        private readonly int rate;
        private readonly bool isActive;
        private readonly bool isPending;
        private readonly bool isPayed;
        private readonly Cottage cottage;
        private readonly Client client;
        private readonly decimal price;

        public Booking(
            string id,
            string note, 
            DateTime fromDate,
            DateTime tillDate,
            int rate,
            bool isActive,
            bool isPending,
            bool isPayed,
            Cottage cottage, 
            Client client,
            decimal price)
        {
            this.id = !string.IsNullOrWhiteSpace(id) ? id : Guid.NewGuid().ToString("N");
            this.client = client;
            this.cottage = cottage;
            this.note = note;
            this.fromDate = fromDate;
            this.tillDate = tillDate;
            this.price = price;
            this.rate = rate;
            this.isActive = isActive;
            this.isPending = isPending;
            this.isPayed = isPayed;
        }

        public string Id => id;
        public string Note => note;
        public DateTime FromDate => fromDate;

        public DateTime TillDate
        {
            get
            {
                if (fromDate > tillDate)
                {
                    throw new ArgumentException($"Tilldate must be past {fromDate}");
                }
                return tillDate;
            }
        }

        public int Rate => rate;
        public bool IsActive => isActive;
        public bool IsPending => isPayed;
        public bool IsPayed => isPayed;
        public Cottage Cottage => cottage;
        public Client Client => client;
        public decimal Price => price;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Id: {this.Id},");
            sb.AppendLine($"Note: {this.Note},");
            sb.AppendLine($"From date: {this.FromDate},");
            sb.AppendLine($"Till date: {this.TillDate},");
            sb.AppendLine($"Rate: {this.Rate}");
            sb.AppendLine($"IsActive: {this.IsActive}");
            sb.AppendLine($"IsPending: {this.IsPending}");
            sb.AppendLine($"IsPayed: {this.IsPayed}");
            sb.AppendLine($"Price: {this.Price}");
            sb.AppendLine("");
            sb.AppendLine($"Client: {this.Client}");
            sb.AppendLine($"Cottage: {this.Cottage}");
            sb.AppendLine("");

            return sb.ToString();
        }
    }
}
