﻿using System;
using System.Text;

namespace HolidayCottageBooking.Models
{
    public class Cottage
    {
        private readonly string id;
        private readonly string name;
        private readonly string description;
        private readonly int numberOfBeds;
        private readonly int rating;
        private readonly NewList<string> facilities;
        private readonly bool isActive;

        public Cottage(
            string id,
            string name,
            string description,
            int numberOfBeds,
            int rating,
            NewList<string> facilities,
            bool isActive)
        {
            this.id = !string.IsNullOrWhiteSpace(id) ? id : Guid.NewGuid().ToString("N");
            this.name = name;
            this.description = description;
            this.numberOfBeds = numberOfBeds;
            this.rating = rating;
            this.facilities = facilities;
            this.isActive = isActive;
        }

        public string Id => id;
        public string Name
        {
            get
            {
                if (string.IsNullOrWhiteSpace(name))
                {
                    throw new ArgumentException($"Name cannot be empty");
                }
                return name;
            }
        }
        public string Description
        {
            get
            {
                if (string.IsNullOrWhiteSpace(description))
                {
                    throw new ArgumentException($"Description cannot be empty");
                }
                return description;
            }
        }
        public int NumberOfBeds => numberOfBeds;
        public int Rating => rating;
        public NewList<string> Facilities => facilities;
        public bool IsActive => isActive;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Id: {this.Id},");
            sb.AppendLine($"Name: {this.Name},");
            sb.AppendLine($"Description: {this.Description},");
            sb.AppendLine($"Number of beds: {this.NumberOfBeds},");
            sb.AppendLine($"Rating: {this.Rating}");
            sb.AppendLine($"Facilities: {this.Facilities}");
            sb.AppendLine($"IsActive: {this.IsActive}");

            return sb.ToString();
        }
    }
}
