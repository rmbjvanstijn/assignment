﻿using System;
using System.Text;

namespace HolidayCottageBooking.Models
{
    public class Price
    {
        private readonly string id;
        private readonly decimal pricePerNight;
        private readonly DateTime fromDate;
        private readonly DateTime tillDate;
        private readonly bool isActive;
        private readonly string cottageId;

        public Price(
            string id,
            decimal pricePerNight,
            DateTime fromDate,
            DateTime tillDate,
            bool isActive,
            string cottageId)
        {
            this.id = !string.IsNullOrWhiteSpace(id) ? id : Guid.NewGuid().ToString("N");
            this.pricePerNight = pricePerNight;
            this.fromDate = fromDate;
            this.tillDate = tillDate;
            this.isActive = isActive;
            this.cottageId = cottageId;
        }

        public string Id => id;
        public decimal PricePerNight => pricePerNight;
        public DateTime FromDate => fromDate;
        public DateTime TillDate
        {
            get
            {
                if (fromDate > tillDate)
                {
                    throw new ArgumentException($"Tilldate must be past {fromDate}");
                }
                return tillDate;
            }
        }
        public bool IsActive => isActive;
        public string CottageId => cottageId;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Id: {this.Id},");
            sb.AppendLine($"Price per night: {this.PricePerNight},");
            sb.AppendLine($"From date: {this.FromDate},");
            sb.AppendLine($"Till date: {this.TillDate},");
            sb.AppendLine($"IsActive: {this.IsActive}");
            sb.AppendLine($"CottageId: {this.CottageId}");

            return sb.ToString();
        }
    }
}
