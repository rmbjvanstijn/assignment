﻿using System.Collections.Generic;

namespace HolidayCottageBooking.Models
{
    public class NewList<T> : List<T>
    {
        public override string ToString()
        {
            var newString = string.Empty;
            foreach (var item in this)
            {
                newString += string.IsNullOrEmpty(newString) ? item.ToString() : $", {item}";
            }
            return newString;
        }
    }
}
